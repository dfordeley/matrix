﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace Matrix
{
   public class MProgram
    {
        static HttpClient client = new HttpClient();
        static int ArraySize = 3; 
        static  void Main(string[] args)
        {
            Console.WriteLine("Matrix Product");
            Execute().Wait();
        }
        /// <summary>
        /// Execute Main
        /// </summary>
        /// <returns></returns>
        static async Task Execute()
        {

            await InitializeData();

            var firstMatrix = await InitializeArray("A");
            var secondMatrix = await InitializeArray("B");

            var product = Product(firstMatrix, secondMatrix);


            var hashedString = CreateMD5(product);
            var validationString = await ValidateResult(hashedString);

            Console.WriteLine(validationString);

            Console.ReadKey();
        }
        /// <summary>
        /// Initialize Data
        /// </summary>
        /// <returns></returns>
        public static async Task InitializeData()
        {
            var url = $"https://recruitment-test.investcloud.com/api/numbers/init/{ArraySize}";
           // var a = $"{url}";
            var response = await client.GetAsync(url);
            var result = response.Content.ReadAsStringAsync();
        }
        /// <summary>
        /// Get data from the API
        /// </summary>
        /// <param name="dataset"></param>
        /// <param name="type"></param>
        /// <param name="idx"></param>
        /// <returns></returns>
        public static async Task<int[]> GetData(string dataset, string type, int idx)
        {
            var url = $"https://recruitment-test.investcloud.com/api/numbers/{dataset}/{type}/{idx}";
            ApiResponse parsed = new ApiResponse();
            try
            {
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();
                parsed = JsonConvert.DeserializeObject<ApiResponse>(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return parsed.Value;
        }
        /// <summary>
        /// Initiliaze array
        /// </summary>
        /// <param name="dataset"></param>
        /// <returns></returns>
        public static async Task<int[,]> InitializeArray(string dataset)
        {
            int[,] matrix = new int[ArraySize, ArraySize];

            for (int i = 0; i < ArraySize; i++)
            {
                var response = await GetData(dataset, "row", i);

                for (int j = 0; j < ArraySize; j++)
                {
                    matrix[i, j] = response[j];
                }
            }

            return matrix;
        }
        /// <summary>
        /// Submit hashed result 
        /// </summary>
        /// <param name="hashedValue"></param>
        /// <returns></returns>
        public static async Task<string> ValidateResult(string hashedValue)
        {
            var url = $"https://recruitment-test.investcloud.com/api/numbers/validate";

            StringContent data = new StringContent(hashedValue, Encoding.UTF8, "application/json");

            var result = string.Empty;

            try
            {
                var response = await client.PostAsync(url, data);

                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public static int[,] Initialize(int rows, int columns)
        {
            int[,] matrix = new int[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    matrix[i, j] = j + 1;
                }
            }

            return matrix;
        }
        /// <summary>
        /// Find the product of 2 matrix
        /// </summary>
        /// <param name="firstMatrix"></param>
        /// <param name="secondMatrix"></param>
        /// <returns></returns>
        public static string Product(int[,] firstMatrix, int[,] secondMatrix)
        {
            int rows = firstMatrix.GetLength(0);
            int columns = firstMatrix.GetLength(1);

            int[,] productMatrix = new int[rows, columns];

            StringBuilder result = new StringBuilder();

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    var product = 0.0;// string.Empty;

                    for (int k = 0; k < columns; k++)
                    {
                        var temp = firstMatrix[i, k] * secondMatrix[k, j];

                        product += temp;//.ToString();
                    }
                    result.Append(product);
                    //result += product.ToString();
                    //productMatrix[i, j] = product;
                }
            }

            //Console.WriteLine(result);

            //return productMatrix;
            return result.ToString();
        }

        public static void Display(int[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int columns = matrix.GetLength(1);

            for (int i = 0; i < rows; i++)
            {
                Console.WriteLine("");
                for (int j = 0; j < columns; j++)
                {
                    Console.Write($"{matrix[i, j]},");
                }
            }

        }

        /// <summary>
        /// MD5 Hash 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    
    }

    public class ApiResponse
    {
        public int[] Value { get; set; }
        public string Cause { get; set; }
        public bool Success { get; set; }
    }
}

