﻿using Matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MatrixTest
{
    public class MatrixTests
    {
        [Fact]
        public void TestProduct_ShouldPass()
        {
            int[,] A = new int[3, 3] {
                                 {0, -2, -2} ,   
                                 {-2, -2, 0} ,   
                                 {-2, 0, 1}    };
            int[,] B = new int[3, 3] {
                                 {-2, -1, 0} ,
                                 {-1, 0, 2} ,
                                 {0, 2, 2}    };
            string C = "2-4-862-4442";

            var result = MProgram.Product(A, B);

            Assert.Equal(C, result);
        }
        
        [Fact]
        public async void InitializeArray_ShouldPass()
        {

            string dataset = "2-4-862-4442";
            int[,] expectedResult = new int[3, 3] {
                                 {2, -4, -8} ,
                                 {6, 2, -4} ,
                                 {4, 4, 2}    };

            var result = await MProgram.InitializeArray(dataset);

            Assert.Equal(expectedResult, result);
        }
        [Fact]
        public void TestProduct_ShouldFail()
        {
            int[,] A = new int[3, 3] {
                                 {0, -2, -2} , 
                                 {-2, -2, 0} ,
                                 {-2, 0, 1}    };
            int[,] B = new int[3, 3] {
                                 {-2, -1, 0} ,
                                 {-1, 0, 2} ,
                                 {0, 2, 2}    };
            string C = "2-4-862-4445";

            var result = MProgram.Product(A, B);

            Assert.NotEqual(C, result);
        }
    }
}
